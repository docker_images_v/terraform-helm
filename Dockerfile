FROM alpine:3.18

ENV PATH /root/yandex-cloud/bin:${PATH}

COPY .terraformrc /root/.terraformrc

RUN apk add --update --no-cache wget git curl bash unzip yq &&\
    wget https://hashicorp-releases.yandexcloud.net/terraform/1.8.3/terraform_1.8.3_linux_amd64.zip &&\
    unzip terraform_1.8.3_linux_amd64.zip &&\
    mv terraform /usr/bin/terraform &&\
    wget https://get.helm.sh/helm-v3.14.4-linux-amd64.tar.gz -O - | tar -xz &&\
    mv linux-amd64/helm /usr/bin/helm &&\
    chmod +x /usr/bin/helm &&\
    curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash &&\
    rm -rf terraform_1.8.3_linux_amd64.zip linux-amd64 install.sh /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["tail", "-f", "/dev/null"]
